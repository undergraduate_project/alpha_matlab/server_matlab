classdef Singleton < handle

   properties(Access=private)
      server;
   end
   
   methods(Access=private)
      % Guard the constructor against external invocation.  We only want
      % to allow a single instance of this class.  See description in
      % Singleton superclass.
      function newObj = Singleton()
         % Initialise your custom properties.

         newObj.server = py.sw_server.SteeringWheelServer();
         newObj.server.start_communication();
      end
   end
   
   methods(Static)
      % Concrete implementation.  See Singleton superclass.
      function obj = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            obj = Singleton();
            uniqueInstance = obj;
         else
            obj = uniqueInstance;
         end
      end
   end
   
   methods % Public Access
      function [target_angle, curr_angle] = get_angles(obj)
         data = obj.server.get_angles();

         target_angle = double(data(1));
         curr_angle   = double(data(2));
      end  
   end

   methods % Public Access
      function set_torque(obj, torque)
         obj.server.set_torque(torque);

      end  
   end   

   methods % Public Access
      function res = down(obj)
         res = False;

         res = obj.server.down();
      end  
   end   
   
end

