classdef SWBlock < matlab.System
% Python def Server imported from python module matlab_block.py
properties
    server = Singleton.instance();
end



methods(Access = protected)

function [target,curr] = stepImpl(obj, torque)

    %if (nargin == 1)
    %    obj.server.set_torque(torque);
    %end
    
    %res = obj.server.get_actual_angles();
    %if (isnan(res(1)))
    %    return;
    %end
    obj.server.set_torque(torque);

    [target, curr] = obj.server.get_angles();
end


function varargout = getOutputDataTypeImpl(obj)
    varargout{1} = 'double';
    varargout{2} = 'double';
end
function varargout = getOutputSizeImpl(obj)
    varargout{1} = [1 1];
    varargout{2} = [1 1];
end
function varargout = isOutputComplexImpl(obj)
    varargout{1} = false;
    varargout{2} = false;
end
function varargout = isOutputFixedSizeImpl(obj)
    varargout{1} = true;
    varargout{2} = true;
end
end
methods(Static, Access = protected)
function simMode = getSimulateUsingImpl
    simMode = 'Interpreted execution';
end
end
end
