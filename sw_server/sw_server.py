import websockets.sync.server as ws_server
from threading import Thread


class SteeringWheelServer():
    def __init__(self):

        self._protocol  = Protocol()
        self._ws_server = WSServerWrapper(self._protocol)


    def start_communication(self, ip = 'localhost', port = 8687):
        self._ws_server.up(ip, port)       


    def stop_communication(self):
        self._ws_server.down()


    def get_angles(self) -> float:
        return self._protocol.get_angles()
        
    
    def set_torque(self, torque):
        self._protocol.set_torque(torque)


class Protocol():

    REQEST_TYPE   = bytearray.fromhex("01")
    RESPONSE_TYPE = bytearray.fromhex("02")

    MAX_TORQUE = 100

    def __init__(self):

        self._curr_angle = 0.0
        self._target_angle = 0.0

        self._torque = 0


    def set_torque(self, torque):
        self._torque = clamp(-self.MAX_TORQUE, torque, self.MAX_TORQUE)
    
    
    def get_angles(self):   
        return self._target_angle, self._curr_angle


    def process_request(self, msg):

        if msg[0] != self.REQEST_TYPE[0]:
            return False

        self._curr_angle   =  twos_complement(msg[4] << 8 | msg[3], 16) / 10.  
        self._target_angle =  twos_complement(msg[2] << 8 | msg[1], 16) / 10.  

        return True


    def create_response(self) -> bytearray:
        
        msg = bytearray.fromhex("0000000000")
    
        torque = int( clamp( -1000, self._torque, 1000 )  * 10)  # get data from matlab

        if torque < 0:
            torque &= 0xFFFF

        msg[0] = self.RESPONSE_TYPE[0]

        msg[3:4] = [torque & 0x00FF, torque >> 8]
            
        return msg


class WSServerWrapper():

    def __init__(self, protocol):

        self._ip   = None
        self._port = None

        self._protocol = protocol

        self._ws_server = None
        self._ws_thread = None


    def _ws_loop(self):

        with ws_server.serve(self._communication_loop, 
                                          "",
                                          self._port) as self._ws_server:
          
            self._ws_server.serve_forever()


    def _communication_loop(self, wsock):

      while True:
            try:
               req = wsock.recv()

               is_request_correct = self._protocol.process_request(req)
               
               if is_request_correct:
                    
                    resp = self._protocol.create_response()
                    
                    wsock.send(resp)
           
            except TimeoutError:
                print('timeout error')

                continue

            except Exception as e: 
                print('DISCONNECT')
                print(e)
                break 
         
            
    def up(self, ip = 'localhost', port = 8687):

        self._ip   = ip
        self._port = port

        if not self.is_up():
            self._ws_thread = Thread(target = self._ws_loop, daemon = True)
            self._ws_thread.start()


    def down(self):

        if self.is_up():
            self._ws_server.shutdown()
            return True
        else:
            False


    def is_up(self):

        return (self._ws_server is not None and
                self._ws_thread is not None)
    

# helpers ------------------------------------------
    
def clamp(min_value, value, max_value):
    return max(min_value, min(value, max_value))


def twos_complement(value, bits):
    if value & (1 << (bits-1)):
        value -= 1 << bits
    return value